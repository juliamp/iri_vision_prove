// Copyright (C) 2010-2011 Institut de Robotica i Informatica Industrial, CSIC-UPC.
// Author 
// All rights reserved.
//
// This file is part of iri-ros-pkg
// iri-ros-pkg is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
// 
// IMPORTANT NOTE: This code has been generated through a script from the 
// iri_ros_scripts. Please do NOT delete any comments to guarantee the correctness
// of the scripts. ROS topics can be easly add by using those scripts. Please
// refer to the IRI wiki page for more information:
// http://wikiri.upc.es/index.php/Robotics_Lab

#ifndef _vision_prove_alg_node_h_
#define _vision_prove_alg_node_h_

#include <iri_base_algorithm/iri_base_algorithm.h>
#include "vision_prove_alg.h"
#include <iostream>
// [publisher subscriber headers]
#include <humanoid_common_msgs/humanoid_motionActionGoal.h>
#include <sensor_msgs/JointState.h>
#include <humanoid_common_msgs/tag_pose_array.h>
#include <geometry_msgs/Twist.h>
#include <trajectory_msgs/JointTrajectoryPoint.h>

// [service client headers]
#include <humanoid_common_msgs/set_walk_params.h>
#include <humanoid_common_msgs/set_servo_modules.h>

// [action server client headers]
#include <humanoid_common_msgs/humanoid_motionAction.h>
#include <control_msgs/JointTrajectoryAction.h>
#include <actionlib/client/simple_action_client.h>
#include <actionlib/client/terminal_state.h>
#include <humanoid_common_msgs/humanoid_follow_targetAction.h>
typedef enum {IDLE_JT,SRV_INIT_JT,ACTION_INIT_JT,FINISH_JT} states_JT;
typedef enum {IDLE_HT,SRV_INIT_HT,ACTION_INIT_HT,MSG_HT,FINISH_HT} states_HT;
typedef enum {IDLE_WK,SRV_INIT_WK,SRV_WK_PARAMS,MSG_WK,FINISH_WK} states_WK;
typedef enum {IDLE_AC,SRV_INIT_AC,ACTION_INIT_AC,FINISH_AC} states_AC;
typedef enum {MOVE_HEAD_R,MOVE_HEAD_L} states_MH;
typedef enum {INIT,JT_FCTN,CASE_START,DETECT_QR,QR_DETECT,N_QR_DETECT,TR_WK,VISION_FINISH,SAVE_QR,QR_2,FRST_SRCH,SCND_SRCH,THRD_SRCH,FINISH} states;
/**
 * \brief IRI ROS Specific Algorithm Class
 *
 */
class VisionProveAlgNode : public algorithm_base::IriBaseAlgorithm<VisionProveAlgorithm>
{
  private:
    // [publisher attributes]

    ros::Publisher head_target_publisher_;
    ros::Publisher cmd_vel_publisher_;
    geometry_msgs::Twist cmd_vel_Twist_msg_;
    trajectory_msgs::JointTrajectoryPoint head_target_JointTrajectoryPoint_msg_;


    // [subscriber attributes]
    ros::Subscriber joint_states_subscriber_;
    void joint_states_callback(const sensor_msgs::JointState::ConstPtr& msg);
    pthread_mutex_t joint_states_mutex_;
    void joint_states_mutex_enter(void);
    void joint_states_mutex_exit(void);

    ros::Subscriber qr_pose_subscriber_;
    void qr_pose_callback(const humanoid_common_msgs::tag_pose_array::ConstPtr& msg);
    pthread_mutex_t qr_pose_mutex_;
    void qr_pose_mutex_enter(void);
    void qr_pose_mutex_exit(void);
    

    // [service attributes]

    // [client attributes]
    ros::ServiceClient set_walk_params_client_;
    humanoid_common_msgs::set_walk_params set_walk_params_srv_;

    ros::ServiceClient set_servo_modules_client_;
    humanoid_common_msgs::set_servo_modules set_servo_modules_srv_;

  
    // [action server attributes]

    // [action client attributes]
    actionlib::SimpleActionClient<humanoid_common_msgs::humanoid_motionAction> motion_action_client_;
    humanoid_common_msgs::humanoid_motionGoal motion_action_goal_;
    bool motion_actionMakeActionRequest();
    void motion_actionDone(const actionlib::SimpleClientGoalState& state,  const humanoid_common_msgs::humanoid_motionResultConstPtr& result);
    void motion_actionActive();
    void motion_actionFeedback(const humanoid_common_msgs::humanoid_motionFeedbackConstPtr& feedback);

    actionlib::SimpleActionClient<control_msgs::JointTrajectoryAction> joint_trajectory_client_;
    control_msgs::JointTrajectoryGoal joint_trajectory_goal_;
    bool joint_trajectoryMakeActionRequest();
    void joint_trajectoryDone(const actionlib::SimpleClientGoalState& state,  const control_msgs::JointTrajectoryResultConstPtr& result);
    void joint_trajectoryActive();
    void joint_trajectoryFeedback(const control_msgs::JointTrajectoryFeedbackConstPtr& feedback);

    actionlib::SimpleActionClient<humanoid_common_msgs::humanoid_follow_targetAction> head_follow_target_client_;
    humanoid_common_msgs::humanoid_follow_targetGoal head_follow_target_goal_;
    bool head_follow_targetMakeActionRequest();
    void head_follow_targetDone(const actionlib::SimpleClientGoalState& state,  const humanoid_common_msgs::humanoid_follow_targetResultConstPtr& result);
    void head_follow_targetActive();
    void head_follow_targetFeedback(const humanoid_common_msgs::humanoid_follow_targetFeedbackConstPtr& feedback);


   /**
    * \brief config variable
    *
    * This variable has all the driver parameters defined in the cfg config file.
    * Is updated everytime function config_update() is called.
    */
    Config config_;
    bool inst;
    float PI;
    int a;
    std::string id;
	std::string id1;
	std::string id2;
	std::string QR_actual;
    float angle_spin;
	float angle_spin1;
	float angle_spin2;
	float angle_spin_gir;
    int i;
    int i_pan;
    int i_tilt;
    float angle_pan;
    float angle_tilt;
    std::vector<std::string> exists; 
    int sign;
    states_JT current_state_JT;
    states_HT current_state_HT;
    states_WK current_state_WK;
	states_AC current_state_AC;
    states_MH current_state_MH;
    states current_state;
    ros::Duration wait_timeout;
    ros::Time start_wait_time;
    float angle;
    bool start;
    bool cancel;
    bool detect_QR;
    bool e_JTMAR;
    bool e_HFT;
    int n;
    float angle_pan_ant;
    float angle_tilt_ant;
    float X;
    float Y;
    float Z;
	float X1;
    float Y1;
    float Z1;
	float X2;
    float Y2;
    float Z2;
    float angle_x;
    float angle_y;
	float angle_x1;
    float angle_y1;
	float angle_x2;
    float angle_y2;
    int n_QR;
    bool cancel_JT;
    bool cancel_HT;
    bool cancel_WK;
	bool cancel_AC;
    bool start_JT;
    bool start_HT;
    bool start_WK;
	bool start_AC;
    float c_pan;
    float c_tilt_r;
	std::string  id_watched;
	std::vector<std::string> ids;
	std::vector<float> x_angle;
	std::vector<float> y_angle;
	std::vector<std::string> watching_QR;
	int k;
	int o;
	float c_tilt_l;
	int y;
	float angle_pan_act;
	float angle_tilt_act;
	int pos;


  public:
   /**
    * \brief Constructor
    * 
    * This constructor initializes specific class attributes and all ROS
    * communications variables to enable message exchange.
    */
    VisionProveAlgNode(void);

   /**
    * \brief Destructor
    * 
    * This destructor frees all necessary dynamic memory allocated within this
    * this class.
    */
    ~VisionProveAlgNode(void);

  protected:
   /**
    * \brief main node thread
    *
    * This is the main thread node function. Code written here will be executed
    * in every node loop while the algorithm is on running state. Loop frequency 
    * can be tuned by modifying loop_rate attribute.
    *
    * Here data related to the process loop or to ROS topics (mainly data structs
    * related to the MSG and SRV files) must be updated. ROS publisher objects 
    * must publish their data in this process. ROS client servers may also
    * request data to the corresponding server topics.
    */
    void mainNodeThread(void);

   /**
    * \brief dynamic reconfigure server callback
    * 
    * This method is called whenever a new configuration is received through
    * the dynamic reconfigure. The derivated generic algorithm class must 
    * implement it.
    *
    * \param config an object with new configuration from all algorithm 
    *               parameters defined in the config file.
    * \param level  integer referring the level in which the configuration
    *               has been changed.
    */
    void node_config_update(Config &config, uint32_t level);

   /**
    * \brief node add diagnostics
    *
    * In this abstract function additional ROS diagnostics applied to the 
    * specific algorithms may be added.
    */
    void addNodeDiagnostics(void);

    // [diagnostic functions]
    
    // [test functions]

    void start_wait_timer(void);
    bool JT(float pos_pan,float pos_tilt,float vel_pan,float vel_tilt,bool start_JT,bool cancel_JT);
    bool HT(float pos_pan,float pos_tilt,bool start_HT);
    bool WK(float linear_x,float linear_y,float angular_z,bool start_WK, bool cancel_WK);
    bool AC(int type_action,bool start_AC,bool cancel_AC);
    bool QR_exists(std::vector<std::string> exists, std::string id);
	bool cerca_QR(std::string id_to_find);
	int centering(std::vector<float> angle_x);
	float angle_spin_fctn(std::string id1);
	std::vector<int> find_QR(std::vector<std::string> ex, std::vector<std::string> ides);
	std::vector<int> posicions;
};

#endif
