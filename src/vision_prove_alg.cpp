#include "vision_prove_alg.h"

VisionProveAlgorithm::VisionProveAlgorithm(void)
{
  pthread_mutex_init(&this->access_,NULL);
}

VisionProveAlgorithm::~VisionProveAlgorithm(void)
{
  pthread_mutex_destroy(&this->access_);
}

void VisionProveAlgorithm::config_update(Config& config, uint32_t level)
{
  this->lock();

  // save the current configuration
  this->config_=config;
  
  this->unlock();
}

// VisionProveAlgorithm Public API
