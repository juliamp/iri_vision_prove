#include "vision_prove_alg_node.h"
#include <cmath>
#include <iostream>
VisionProveAlgNode::VisionProveAlgNode(void) :
  algorithm_base::IriBaseAlgorithm<VisionProveAlgorithm>(),
  motion_action_client_("motion_action", true),
  joint_trajectory_client_("joint_trajectory", true),
  head_follow_target_client_("head_follow_target", true)
{
  //init class attributes if necessary
  this->loop_rate_ = 10;//in [Hz]
  PI=3.14159265;
  //find real angle pan_tilt
  i_pan=0;
  i_tilt=0;
  angle_pan=0.0;
  angle_tilt=0.0;
  sign=0;
  //QR
  detect_QR=false;
  angle_spin=0.0;
  angle_spin1=0.0;
  angle_spin2=0.0;
  angle_spin_gir=0.0;
  id="";
  id1="";
  id2="";
  this->current_state=INIT;
  //this->current_state=N_QR_DETECT;
  this->current_state_HT=IDLE_HT;
  this->current_state_WK=IDLE_WK;
  this->current_state_JT=IDLE_JT;
  this->current_state_AC=IDLE_AC;
	this->current_state_MH=MOVE_HEAD_L;
  //this->current_state_JT=IDLE_JT;
  //init_starts
  start_JT=true;
  start_HT=true;
  start_WK=true;
	start_AC=true;
  //init tracking
  angle_pan_ant=0.0;
  angle_tilt_ant=0.0;
  X=0.0;
  Y=0.0;
  Z=0.0;
  X1=0.0;
  Y1=0.0;
  Z1=0.0; 
  X2=0.0;
  Y2=0.0;
  Z2=0.0;
  n_QR=0;
  QR_actual="";
  c_pan=0.15;
  c_tilt_r=0.01;
  std::vector<std::string> exists;
  std::vector<std::string> watching_QR;
  std::vector<std::string> ids;
  std::vector<float> x_angle;
  std::vector<float> y_angle;
  std::vector<int> posicions;
  id_watched="";
  k=0;
  n=0;
  o=0;
  c_tilt_l=0.01;
  y=0;
  angle_pan_act=0.0;
  angle_tilt_act=0.0;
  pos=0;
  //this->private_node_handle_.getParam("max_num_retries",this->config.max_num_retries);

  // [init publishers]
  this->cmd_vel_publisher_ = this->public_node_handle_.advertise<geometry_msgs::Twist>("cmd_vel", 1);
  this->head_target_publisher_ = this->public_node_handle_.advertise<trajectory_msgs::JointTrajectoryPoint>("head_target", 1);
  this->head_target_JointTrajectoryPoint_msg_.positions.resize(2);
  this->head_target_JointTrajectoryPoint_msg_.velocities.resize(1);
  // [init subscribers]
  this->joint_states_subscriber_ = this->public_node_handle_.subscribe("joint_states", 1, &VisionProveAlgNode::joint_states_callback, this);
  pthread_mutex_init(&this->joint_states_mutex_,NULL);

  this->qr_pose_subscriber_ = this->public_node_handle_.subscribe("qr_pose", 1, &VisionProveAlgNode::qr_pose_callback, this);
  pthread_mutex_init(&this->qr_pose_mutex_,NULL);

  
  // [init services]

  // [init clients]
  set_walk_params_client_ = this->public_node_handle_.serviceClient<humanoid_common_msgs::set_walk_params>("set_walk_params");
  set_servo_modules_client_ = this->public_node_handle_.serviceClient<humanoid_common_msgs::set_servo_modules>("set_servo_modules");
  
  // [init action servers]

  // [init action clients]
  
  //head_follow
  head_follow_target_goal_.pan_range.resize(2);
  head_follow_target_goal_.tilt_range.resize(2);
  head_follow_target_goal_.pan_range[0] = PI;
  head_follow_target_goal_.pan_range[1] = -PI;
  head_follow_target_goal_.tilt_range[0] = PI;
  head_follow_target_goal_.tilt_range[1] = -PI;
  
  //joint_trajectory
  joint_trajectory_goal_.trajectory.points.resize(1);
  joint_trajectory_goal_.trajectory.joint_names.resize(2);
  joint_trajectory_goal_.trajectory.points[0].positions.resize(2);
  joint_trajectory_goal_.trajectory.points[0].velocities.resize(2);
  joint_trajectory_goal_.trajectory.points[0].accelerations.resize(2);
 
 
  //head_target
  this->head_target_JointTrajectoryPoint_msg_.positions.resize(2);
  this->head_target_JointTrajectoryPoint_msg_.velocities.resize(2);

	
}

VisionProveAlgNode::~VisionProveAlgNode(void)
{
  // [free dynamic memory]
  pthread_mutex_destroy(&this->joint_states_mutex_);
  pthread_mutex_destroy(&this->qr_pose_mutex_);
}

void VisionProveAlgNode::mainNodeThread(void)
{

 // [fill msg structures]
  // Initialize the topic message structure
  //this->motion_action_humanoid_motionActionGoal_msg_.data = my_var;

 // [fill srv structure and make request to the server]
  //set_walk_params_srv_.request.data = my_var;
  //ROS_INFO("VisionProveAlgNode:: Sending New Request!");
  //if (set_walk_params_client_.call(set_walk_params_srv_))
  //{
    //ROS_INFO("VisionProveAlgNode:: Response: %s", set_walk_params_srv_.response.result);
  //}
  //else
  //{
    //ROS_INFO("VisionProveAlgNode:: Failed to Call Server on topic set_walk_params ");
  //}
  // [publish messages]
  // Uncomment the following line to publish the topic message
  //this->motion_action_publisher_.publish(this->motion_action_humanoid_motionActionGoal_msg_);
  // [fill action structure and make request to the action server]
  // variable to hold the state of the current goal on the server
  //


  //this->alg_.lock();
  //JT(1.5,0.0,0.1,0.1,true,false); 
  //HT(1.5,0.0,true,false); 
  //WK(0.0,0.0,0.05,true,false); 
  //AC(2,true,false);
	/*if(cerca_QR("")){
				ROS_INFO("Founded");
				
	}
	else{
			ROS_INFO("Tying to find QR");
	}*/
  switch (this->current_state){
	case INIT:
		ROS_INFO("INIT");
		if (config_.INIT){
			this->current_state=JT_FCTN;
			if(!JT(config_.pan_pos_init,config_.tilt_pos_init,config_.pan_vel_init,config_.tilt_vel_init,true,false)){				
				this->current_state=JT_FCTN;
				ROS_INFO("DarwinVisionProve: TRYING THE JT(0.0,0.3,2,2,true,false)");
			}
		 	else{
				ROS_INFO("DarwinVisionProve:JT(0.0,0.3,2,2,true,false) SUCCESSFUL::NEXT_STATE");
				this->start_JT=true;	
				this->current_state=CASE_START;
			}
		config_.INIT=false;
		ROS_INFO("DarwinVisionProve: INIT");
		}
		else {
			this->current_state=INIT;
			ROS_INFO("DarwinVisionProve:NO INIT");
		}
	break;
        
  case JT_FCTN:
	ROS_INFO("JT_FCTN");
	if(!JT(config_.pan_pos_init,config_.tilt_pos_init,config_.pan_vel_init,config_.tilt_vel_init,false,false)){
		ROS_INFO("DarwinVisionProve: TRYING THE JT(0.0,0.3,2,2,true,false) ");
		this->current_state=JT_FCTN;
	}
	else{
		ROS_INFO("DarwinVisionProve: JT(0.0,0.3,2,2,true,false) SUCCESSFUL::NEXT_STATE");
		this->start_JT=true;
		this->current_state=CASE_START;
	}
		
	break;

case CASE_START:
	ROS_INFO("CASE_START");
	//if (new_data)
	if (o<config_.o){
		if(this->n_QR==1){
				this->current_state=QR_DETECT;
				ROS_INFO("QR DETECTED_NEX STATE: QR_DETECT ");
				exists.push_back(ids[0]);
				o=0;				
		}
		else{
			this->current_state=CASE_START;
			ROS_INFO("QR NOT DETECTED <10 ");
			o+=1;
			ROS_INFO("o: %i",o);
		}
		this->angle_pan_act=angle_pan_ant;
		this->angle_tilt_act=angle_tilt_ant;		
	}
	else{
		if (this->n_QR==0){
				/*//ESTAT DE CERCA//FRST_SRCH
				if(cerca_QR("")){					
				ROS_INFO( "IDS.SIZE()");
				ROS_INFO_STREAM(ids.size());
					if (ids.size()==1){
						this->current_state=CASE_START;
						ROS_INFO( "1 QR DETECTED ");
					}
					else{
						this->current_state=CASE_START;
					}
				}
				ROS_INFO( "SEARCHING QR");*/
				this->current_state=FRST_SRCH;
		}
		else if (this->n_QR==1){
			exists.push_back(ids[0]);
			ROS_INFO( "ONE QR DETECTED");
			o=0;
			this->current_state=QR_DETECT;
		}

		else {
			if (centering(x_angle)!=-1){
				int position=centering(x_angle);
				ROS_INFO("POS: %i",position);
				exists.push_back(ids[position]);
				this->current_state=QR_DETECT;
				ROS_INFO( "TRYING TO CENTER");
				o=0;
			}
			else{
				this->current_state=CASE_START;
				ROS_INFO( "CENTERING");
			}
		}
	o+=1;
	}
	break;

case FRST_SRCH:

	 if (cerca_QR("")){
			if (this->n_QR==1){
				exists.push_back(id1);
				ROS_INFO("FRST_SRCH:ONE QR DETECTED");
				this->current_state=QR_DETECT;
			}
			else if (this->n_QR==0){
				ROS_INFO("FRST_SRCH:NONE QR DETECTED");
				this->current_state=FINISH;
			}
			else{
				int position=centering(x_angle);
				exists.push_back(ids[position]);
				ROS_INFO("FRST_SRCH: MORE THAN ONE QR DETECTED");
				this->current_state=QR_DETECT;
			}
			this->current_state_MH=MOVE_HEAD_L;
	
	o=0;
	}
	else{
		if (o<config_.o){
			this->current_state=FRST_SRCH;
		}
		else{
			this->current_state=FINISH;
		}
		o+=1;
	}

case QR_DETECT:
	this->angle_spin=angle_spin_fctn(exists[exists.size()-1]);
	ROS_INFO("QR DETECT, SPINING THE NEXT id");
	ROS_INFO_STREAM(exists[exists.size()-1]);
  	ROS_INFO("angle_spin: %f",this->angle_spin);
		if(!JT(this->angle_spin,0.1,config_.pan_vel_spin,config_.tilt_vel_spin,true,false)){//this->angle_tilt
				ROS_INFO("DarwinVisionProve: active QR_DETECT ");
				this->current_state=QR_DETECT;                
		}
		else{
			ROS_INFO("DarwinVisionProve: FINISH_QR, NEXT STATE ");
			this->current_state=SAVE_QR;
			this->current_state_HT=IDLE_HT;
    }
  break;

case SAVE_QR:
	ROS_INFO("SAVE_QR");
	if (o<config_.o){
			if (this->n_QR==1){
				exists.push_back(ids[0]);
				ROS_INFO("SAVING THE NEXT QR:");
				ROS_INFO_STREAM(exists[exists.size()-1]);
				ROS_INFO("QR DETECTED_ NEX STATE: TR_WK");
				this->current_state=TR_WK;
				this->id=exists[exists.size()-1];
				ROS_INFO("this->id");
				ROS_INFO_STREAM(this->id);
				o=0;	
			}
			else{
				this->current_state=SAVE_QR;
				ROS_INFO("QR NOT DETECTED <10 ");
				o+=1;
				ROS_INFO("o: %i",o);
			}

		this->angle_pan_act=angle_pan_ant;
		this->angle_tilt_act=angle_tilt_ant;		
		
	}
	else{
		if (this->n_QR==0){
			
				if(cerca_QR("")){	
								
				ROS_INFO( "IDS.SIZE()");
				ROS_INFO_STREAM(ids.size());
					if (ids.size()==1){
						this->current_state=SAVE_QR;
						ROS_INFO( "1 QR DETECTED ");
					}
				}
				else{
						this->current_state=SAVE_QR;
					}
				//this->current_state=SCND_SRCH;
		ROS_INFO( "SEARCHING QR");		
		}
		else if (this->n_QR==1){
				exists.push_back(ids[0]);
				ROS_INFO("SAVING THE NEXT QR:");
				ROS_INFO_STREAM(exists[exists.size()-1]);
				this->current_state=TR_WK;
				this->id=exists[exists.size()-1];
				ROS_INFO("this->id");
				ROS_INFO_STREAM(this->id);
				ROS_INFO("ONE QR DETECTED");
				o=0;
		}

		else {
			  posicions=find_QR(exists,ids);
			ROS_INFO("SEARCHING POSICIONS");
        if (posicions.size()!=0){
          for (k = 0; k <posicions.size() ; ++k){       
		   						x_angle.push_back(this->x_angle[posicions[k]]);										
				  }
				  int position=centering(x_angle);
				  ROS_INFO("POS: %i",position);
				  this->current_state=TR_WK;
				  exists.push_back(ids[position]);
				  ROS_INFO( "MORE THAN ONE ARE A POSSIBILE SOLUTION ");
				  ROS_INFO("SAVING THE NEXT QR:");
				  ROS_INFO_STREAM(exists[exists.size()-1]);
				  this->id=exists[exists.size()-1];
				  ROS_INFO("this->id");
				  ROS_INFO_STREAM(this->id);
			  }
			else{
				ROS_INFO( " NONE OF THEM ARE A POSSIBLE SOLUTION:FINISSSHHHHHH ");
			}
		}
	o+=1;
	}
	break;


case SCND_SRCH:

	 if (cerca_QR("")){
			if (this->n_QR==1){
				this->current_state=SAVE_QR;
				ROS_INFO("SCND_SRCH:ONE QR DETECTED");
					this->current_state_MH=MOVE_HEAD_L;
			}
			else if (this->n_QR==0){
				ROS_INFO("SCND_SRCH:NONE QR DETECTED");
				this->current_state=FINISH;
			}
			else{

				ROS_INFO("FRST_SRCH: MORE THAN ONE QR DETECTED");
				this->current_state=SAVE_QR;
			}
	
	o=0;
	}
	else{
		if (o<20){
			this->current_state=SCND_SRCH;
			o=+1;
			ROS_INFO("SCND_SRCH o<?");
		}
		else{
			this->current_state=FINISH;
			ROS_INFO("FINISH");
		}
	}
case TR_WK:
			ROS_INFO("QR THAT I AM FOLLOWING ");
			ROS_INFO_STREAM(this->id);
 			ROS_INFO_STREAM(exists[exists.size()-1]);
			this->angle_tilt= angle_tilt_ant - angle_y;
			ROS_INFO_STREAM(exists.size());
			ROS_INFO("TR_WK");
  
			if (angle_x<-config_.angle_x_HT){
				this->angle_pan = angle_pan_ant - angle_x;
				if(!HT(angle_pan,angle_tilt,true)){
					ROS_INFO("right");
					ROS_INFO("angle_pan: %f", angle_pan);
					ROS_INFO("angle_pan_ant: %f", angle_pan_ant);
					ROS_INFO("angle_x: %f", angle_x);
					}
					else{
						this->start_HT=true;
						ROS_INFO("SUCCEEDED");
					}
			}	 		
				
			else if (angle_x>config_.angle_x_HT){
				this->angle_pan = angle_pan_ant - angle_x;
				 	if(!HT(angle_pan,angle_tilt,true)){
						ROS_INFO("right");
						ROS_INFO("angle_pan: %f", angle_pan);
						ROS_INFO("angle_pan_ant: %f", angle_pan_ant);
						ROS_INFO("angle_x: %f", angle_x);		 				
					}
					else{
						this->start_HT=true;
						ROS_INFO("SUCCEEDED");
					} 
			}

			if (angle_y>config_.angle_y_HT){
				this->angle_tilt = angle_tilt_ant - angle_y;
				if(!HT(angle_pan,angle_tilt,true)){	
					ROS_INFO("up");
					ROS_INFO("angle_tilt: %f", angle_tilt);
					ROS_INFO("angle_tilt_ant: %f", angle_tilt_ant);
					ROS_INFO("angle_y: %f", angle_x);
				}
				else{
					this->start_HT=true;
					ROS_INFO("SUCCEEDED");
				}
			}
			else if (angle_y<-config_.angle_y_HT){
				this->angle_tilt = angle_tilt_ant - angle_y;
				if(!HT(angle_pan,angle_tilt,true)){			
					ROS_INFO("down");
					ROS_INFO("angle_tilt: %f", angle_tilt);
					ROS_INFO("angle_pan_ant: %f", angle_pan_ant);
					ROS_INFO("angle_x: %f", angle_x);		
				}
				else{
					this->start_HT=true;
					ROS_INFO("SUCCEEDED");
				}
			} 
		
			if (angle_pan_ant<-config_.angle_min_WK){
					ROS_INFO("angle_pan_ant: %f", angle_pan_ant);
					WK(0.0,0.0,-0.09,true,false);
					ROS_INFO("dreta");
					ROS_INFO("WALKING");
			}
			else if (angle_pan_ant>config_.angle_min_WK){
					ROS_INFO("angle_pan_ant: %f", angle_pan_ant);
					WK(0.0,0.0,+0.09,true,false);
					ROS_INFO("esquerra");
					ROS_INFO("WALKING");
			}
			else {
					ROS_INFO("angle_pan_ant: %f", angle_pan_ant);
					ROS_INFO("FINISH WALKING ");
					config_.INIT=true;
					this->start_JT=true;
				 	this->start_HT=true;
					this->start_WK=true;
					if(WK(0.0,0.0,0.0,false,true)){
						this->current_state_JT=IDLE_JT;
						this->current_state=N_QR_DETECT;
						this->current_state_HT=IDLE_HT;
					}
					else{
						this->current_state=TR_WK; 
					}  
			ROS_INFO_STREAM(id);
			}
			if (ids.size()!=1||id1!=exists[exists.size()-1]){
					this->start_JT=true;
				 	this->start_HT=true;
					this->start_WK=true;
					if(WK(0.0,0.0,0.0,false,true)){
						this->current_state_JT=IDLE_JT;
						this->current_state=THRD_SRCH;
						this->current_state_HT=IDLE_HT;
					}
					else{
						this->current_state=TR_WK; 
					}  
			}
			
	break;

case THRD_SRCH:
	if (cerca_QR(exists[exists.size()-1])){
				if (this->n_QR==1){
					ROS_INFO("SCND_SRCH:ONE QR DETECTED");
					this->current_state=TR_WK;
				}
				else if (this->n_QR==0){
					ROS_INFO("SCND_SRCH:NONE QR DETECTED");
					this->current_state=FINISH;
				}
				else{
					ROS_INFO("FRST_SRCH: MORE THAN ONE QR DETECTED");//
					this->current_state=THRD_SRCH;
				}
	
		o=0;
		}
		else{
			if (o<20){
				this->current_state=THRD_SRCH;
				o=+1;
				ROS_INFO("SCND_SRCH o<?");
			}
			else{
				this->current_state=FINISH;
				ROS_INFO("FINISH");
			}
		}

 case N_QR_DETECT:

		ROS_INFO("N_QR_DETECT");
		ROS_INFO_STREAM(this->id1);
		ROS_INFO_STREAM(exists[exists.size()-1]);
		if (o<config_.o){
			if((this->n_QR==1)&&(exists[exists.size()-1]==this->id1)){
					this->current_state=QR_DETECT;
					ROS_INFO("QR DETECTED_ NEX STATE: QR_DETECT ");
					o=0;				
			}
			else{
				this->current_state=N_QR_DETECT;
				ROS_INFO("QR NOT DETECTED <5 ");
				o+=1;
				ROS_INFO("o: %i",o);

			this->angle_pan_act=angle_pan_ant;
			this->angle_tilt_act=angle_tilt_ant;		
			}
		}
		else {
            if ((this->n_QR == 1)&&(exists[exists.size() - 1] == this->id1)) {
                    n = 0;
                    this->current_state = QR_DETECT;
                    ROS_INFO("QR DETECTED_ NEX STATE: QR_DETECT  ");
                    this->current_state_HT = IDLE_HT;
                    o = 0;                
            }

            else {
                if (cerca_QR(exists[exists.size() - 1])) {
                    if (exists[exists.size() - 1] == this->id1) {
                        this->current_state = QR_DETECT;
                        ROS_INFO(" QR DETECTED ");
                    }
                    else {
                        this->current_state = N_QR_DETECT;
						 ROS_INFO(" REINTENT TO FIND THE QR ");
                    }
                }

                else {
                    this->current_state = N_QR_DETECT;
                }
            }
        }
	o+=1;

	break;

case FINISH:

	ROS_INFO(" FINISH STATE ");
 }		



  /*
  case QR_2:

				if (fabs(angle_x1)<fabs(angle_x2)){
						this->id=id1;
						this->angle_x=angle_x1;
						this->angle_y=angle_y1;
						this->angle_spin=angle_spin1;
						this->angle_pan = angle_pan_ant - this->angle_x;
						this->angle_tilt = angle_tilt_ant - this->angle_y;
						if(!JT(angle_pan,angle_tilt,2,2,true,false)){
						   ROS_INFO_STREAM(id);
						   ROS_INFO("guanya el primer: %f",angle_spin);
						   ROS_INFO("angle_x: %f",angle_x);
						   ROS_INFO("angle_y: %f",angle_y);
						   this->current_state=QR_2;
							
						}
						else{
						    this->current_state=N_QR_DETECT;
						}
				}
				else if (fabs(angle_x1)>fabs(angle_x2)){
					this->id=id2;
					this->angle_x=angle_x2;
					this->angle_y=angle_y2;
					this->angle_spin=an
gle_spin2;
					this->angle_pan = angle_pan_ant - this->angle_x;
					this->angle_tilt = angle_tilt_ant - this->angle_y;
					if(!JT(angle_pan,angle_tilt,2,2,true,false)){
					   ROS_INFO_STREAM(id);
					   ROS_INFO("guanya el primer: %f",angle_spin);
					   ROS_INFO("angle_x: %f",angle_x);
					   ROS_INFO("angle_y: %f",angle_y);
					   this->current_state=QR_2;

					}
					else{       
						this->current_state=N_QR_DETECT;
					}
				}

				
	

		case SAVE_QR:
					if (this->n_QR==1){
										if (QR_exists(exists, id)){
															 ROS_INFO("problem: it has been descodificated before!");
										}
										else{					
												this->current_state=TR_WK;
												exists.push_back(id);
												this->start_WK=true;
										}
					}
					else if (this->n_QR==2){
					
							
					
			
		break;
   

	
//case CANCEL_JT:
	
					
}
/*
   if(!JT(PI,0.1,0.7,0.7,true)){
	ROS_INFO("DarwinVisionProve: active ");
		//this->current_state=QR_DETECT; 
	               
   }
   else{
	ROS_INFO("DarwinVisionProve: FINISH_QR, NEXT STATE ");
        //this->current_state=TR_WK;   
	HT(2.8,0.1,true);
   }
				
  //ROS_INFO("CURRENT_STATE: %i",this->current_state_HT);
  		
  /*
  
//alg_.unlock();*/
 
}

/*  [subscriber callbacks] */
void VisionProveAlgNode::joint_states_callback(const sensor_msgs::JointState::ConstPtr& msg)
{
  //ROS_INFO("VisionProveAlgNode::joint_states_callback: New Message Received");
  //use appropiate mutex to shared variables if necessary
  //this->alg_.lock();
  //this->joint_states_mutex_enter();

   for (i = 0; i <20 ; ++i){
 	if (msg->name[i]=="j_pan"){
		i_pan=i;
        }
	else if (msg->name[i]=="j_tilt"){
		i_tilt=i;
	}
  	angle_pan_ant=(msg->position[i_pan]);//*(180/PI);
  	angle_tilt_ant=(msg->position[i_tilt]);//*(180/PI);
 	//ROS_INFO("angle_pan: %f",angle_pa_ant);
 	//ROS_INFO("angle_tilt: %f",angle_ti_ant);
   }  
  //std::cout << msg->data << std::endl;
  //unlock previously blocked shared variables
  //this->alg_.unlock();
  //this->joint_states_mutex_exit();

}
void VisionProveAlgNode::joint_states_mutex_enter(void)
{
  pthread_mutex_lock(&this->joint_states_mutex_);
}

void VisionProveAlgNode::joint_states_mutex_exit(void)
{
  pthread_mutex_unlock(&this->joint_states_mutex_);
}

void VisionProveAlgNode::qr_pose_callback(const humanoid_common_msgs::tag_pose_array::ConstPtr& msg)
{
	std::vector<std::string> idn;
	std::vector<float> xangle;
  std::vector<float> yangle;
  //ROS_INFO_STREAM(msg->tags.size());
  //ROS_INFO_STREAM(id);
  //bool new_data=true;
  this->n_QR=msg->tags.size();
    for (k = 0; k <msg->tags.size() ; ++k){
				this->X1=msg->tags[k].position.x;
				this->Y1=msg->tags[k].position.y;
				this->Z1=msg->tags[k].position.z;	
				idn.push_back(msg->tags[k].tag_id);
				xangle.push_back(atan2(X1,Z1));
				yangle.push_back(atan2(Y1,Z1));
	}
	this->ids=idn;
	this->x_angle=xangle;
	this->y_angle=yangle;
	if (QR_exists( this->ids, this->id)){
			for (i=0; i<msg->tags.size(); ++i){
					if (ids[i]==this->id){
						this->pos=i;
					}
			}
			if (n_QR!=0){
					angle_x=x_angle[this->pos];
					angle_y=y_angle[this->pos];
					ROS_INFO("this->pos: %i",this->pos);
					ROS_INFO("angle_x: %f",x_angle[this->pos]);
					ROS_INFO_STREAM(this->id);
				ROS_INFO("n_QR: %i",n_QR);
			}
			if (this->n_QR==1){
				this->id1=msg->tags[0].tag_id;

			}
	}
	
	
  
  //use appropiate mutex to shared variables if necessary
  //this->alg_.lock();
  //this->qr_pose_mutex_enter();

  //std::cout << msg->data << std::endl;
  //unlock previously blocked shared variables
  //this->alg_.unlock();
  //this->qr_pose_mutex_exit();
}

void VisionProveAlgNode::qr_pose_mutex_enter(void)
{

  pthread_mutex_lock(&this->qr_pose_mutex_);
}

void VisionProveAlgNode::qr_pose_mutex_exit(void)
{
  pthread_mutex_unlock(&this->qr_pose_mutex_);
}


/*  [service callbacks] */

/*  [action callbacks] */
void VisionProveAlgNode::motion_actionDone(const actionlib::SimpleClientGoalState& state,  const humanoid_common_msgs::humanoid_motionResultConstPtr& result)
{
  alg_.lock();
  if( state == actionlib::SimpleClientGoalState::SUCCEEDED )
    ROS_INFO("VisionProveAlgNode::motion_actionDone: Goal Achieved!");
  else
    ROS_INFO("VisionProveAlgNode::motion_actionDone: %s", state.toString().c_str());


  //copy & work with requested result
  alg_.unlock();
}

void VisionProveAlgNode::motion_actionActive()
{
  alg_.lock();
  //ROS_INFO("VisionProveAlgNode::motion_actionActive: Goal just went active!");
  alg_.unlock();
}

void VisionProveAlgNode::motion_actionFeedback(const humanoid_common_msgs::humanoid_motionFeedbackConstPtr& feedback)
{
  alg_.lock();
  //ROS_INFO("VisionProveAlgNode::motion_actionFeedback: Got Feedback!");

  bool feedback_is_ok = true;

  //analyze feedback
  //my_var = feedback->var;

  //if feedback is not what expected, cancel requested goal
  if( !feedback_is_ok )
  {
    motion_action_client_.cancelGoal();
    //ROS_INFO("VisionProveAlgNode::motion_actionFeedback: Cancelling Action!");
  }
  alg_.unlock();
}

void VisionProveAlgNode::joint_trajectoryDone(const actionlib::SimpleClientGoalState& state,  const control_msgs::JointTrajectoryResultConstPtr& result)
{
  alg_.lock();
  if( state == actionlib::SimpleClientGoalState::SUCCEEDED )
    ROS_INFO("VisionProveAlgNode::joint_trajectoryDone: Goal Achieved!");
  else
    ROS_INFO("VisionProveAlgNode::joint_trajectoryDone: %s", state.toString().c_str());
	
  //copy & work with requested result
  alg_.unlock();
}

void VisionProveAlgNode::joint_trajectoryActive()
{
  alg_.lock();
  //ROS_INFO("VisionProveAlgNode::joint_trajectoryActive: Goal just went active!");
  alg_.unlock();
}

void VisionProveAlgNode::joint_trajectoryFeedback(const control_msgs::JointTrajectoryFeedbackConstPtr& feedback)
{
  alg_.lock();
  //ROS_INFO("VisionProveAlgNode::joint_trajectoryFeedback: Got Feedback!");

  bool feedback_is_ok = true;

  //analyze feedback
  //my_var = feedback->var;

  //if feedback is not what expected, cancel requested goal
  if( !feedback_is_ok )
  {
    joint_trajectory_client_.cancelGoal();
    //ROS_INFO("VisionProveAlgNode::joint_trajectoryFeedback: Cancelling Action!");
  }
  alg_.unlock();
}

void VisionProveAlgNode::head_follow_targetDone(const actionlib::SimpleClientGoalState& state,  const humanoid_common_msgs::humanoid_follow_targetResultConstPtr& result)
{
  alg_.lock();
  if( state == actionlib::SimpleClientGoalState::SUCCEEDED )
    ROS_INFO("VisionProveAlgNode::head_follow_targetDone: Goal Achieved!");
  else
    ROS_INFO("VisionProveAlgNode::head_follow_targetDone: %s", state.toString().c_str());

  //copy & work with requested result
  alg_.unlock();
}

void VisionProveAlgNode::head_follow_targetActive()
{
  alg_.lock();
  //ROS_INFO("VisionProveAlgNode::head_follow_targetActive: Goal just went active!");
  alg_.unlock();
}

void VisionProveAlgNode::head_follow_targetFeedback(const humanoid_common_msgs::humanoid_follow_targetFeedbackConstPtr& feedback)
{
  alg_.lock();
  //ROS_INFO("VisionProveAlgNode::head_follow_targetFeedback: Got Feedback!");

  bool feedback_is_ok = true;

  //analyze feedback
  //my_var = feedback->var;

  //if feedback is not what expected, cancel requested goal
  if( !feedback_is_ok )
  {
    head_follow_target_client_.cancelGoal();
    //ROS_INFO("VisionProveAlgNode::head_follow_targetFeedback: Cancelling Action!");
  }
  alg_.unlock();
}


/*  [action requests] */
bool VisionProveAlgNode::motion_actionMakeActionRequest()
{
  // IMPORTANT: Please note that all mutex used in the client callback functions
  // must be unlocked before calling any of the client class functions from an
  // other thread (MainNodeThread).
  // this->alg_.unlock();
  if(motion_action_client_.isServerConnected())
  {
    //ROS_DEBUG("VisionProveAlgNode::motion_actionMakeActionRequest: Server is Available!");
    //send a goal to the action server
    //motion_action_goal_.data = my_desired_goal;
    motion_action_client_.sendGoal(motion_action_goal_,
                boost::bind(&VisionProveAlgNode::motion_actionDone,     this, _1, _2),
                boost::bind(&VisionProveAlgNode::motion_actionActive,   this),
                boost::bind(&VisionProveAlgNode::motion_actionFeedback, this, _1));
    // this->alg_.lock();
    // ROS_DEBUG("VisionProveAlgNode::MakeActionRequest: Goal Sent.");
    return true;
  }
  else
  {
    // this->alg_.lock();
    // ROS_DEBUG("VisionProveAlgNode::motion_actionMakeActionRequest: HRI server is not connected");
    return false;
  }
}

bool VisionProveAlgNode::joint_trajectoryMakeActionRequest()
{
  // IMPORTANT: Please note that all mutex used in the client callback functions
  // must be unlocked before calling any of the client class functions from an
  // other thread (MainNodeThread).
  // this->alg_.unlock();
  if(joint_trajectory_client_.isServerConnected())
  	{

	ROS_DEBUG("VisionProveAlgNode::joint_trajectoryMakeActionRequest: Server is Available!");
    	//send a goal to the action server
    	//joint_trajectory_goal_.data = my_desired_goal;
    		joint_trajectory_client_.sendGoal(joint_trajectory_goal_,
                boost::bind(&VisionProveAlgNode::joint_trajectoryDone,     this, _1, _2),
                boost::bind(&VisionProveAlgNode::joint_trajectoryActive,   this),
                boost::bind(&VisionProveAlgNode::joint_trajectoryFeedback, this, _1));
    	// this->alg_.lock();
    	// ROS_DEBUG("VisionProveAlgNode::MakeActionRequest: Goal Sent.");
    	return true;
	}
  	else
  	{
   	 // this->alg_.lock();
  	 //ROS_DEBUG("VisionProveAlgNode::joint_trajectoryMakeActionRequest: HRI server is not connected");
    return false;
  }
}

bool VisionProveAlgNode::head_follow_targetMakeActionRequest()
{
  // IMPORTANT: Please note that all mutex used in the client callback functions
  // must be unlocked before calling any of the client class functions from an
  // other thread (MainNodeThread).
  // this->alg_.unlock();
  if(head_follow_target_client_.isServerConnected())
  {
    //ROS_DEBUG("VisionProveAlgNode::head_follow_targetMakeActionRequest: Server is Available!");
    //send a goal to the action server
	  		head_follow_target_client_.sendGoal(head_follow_target_goal_,
		        boost::bind(&VisionProveAlgNode::head_follow_targetDone,     this, _1, _2),
		        boost::bind(&VisionProveAlgNode::head_follow_targetActive,   this),
		        boost::bind(&VisionProveAlgNode::head_follow_targetFeedback, this, _1));
    // this->alg_.lock();
    // ROS_DEBUG("VisionProveAlgNode::MakeActionRequest: Goal Sent.");
    return true;
  }
  else
  {
    // this->alg_.lock();
    // ROS_DEBUG("VisionProveAlgNode::head_follow_targetMakeActionRequest: HRI server is not connected");
    return false;
  }
}


void VisionProveAlgNode::node_config_update(Config &config, uint32_t level)
{
  this->alg_.lock();
  this->config_=config;
  if(config.START)
     config.START=false;
  if(config.CANCEL)
     config.CANCEL=false;
  if(config.INIT)
     config.INIT=false;
  this->alg_.unlock();
}

void VisionProveAlgNode::addNodeDiagnostics(void)
{
}

/* main function */
int main(int argc,char *argv[])
{
  return algorithm_base::main<VisionProveAlgNode>(argc, argv, "vision_prove_alg_node");
}

bool VisionProveAlgNode::JT(float pos_pan,float pos_tilt,float vel_pan,float vel_tilt,bool startJT,bool cancelJT){ 
	this->start_JT=startJT;
	this->cancel_JT=cancelJT;
	switch(this->current_state_JT)
	{
		case IDLE_JT:
			if(this->start_JT){ 
				ROS_INFO("DarwinVisionProve: START_JT");
				this->current_state_JT=SRV_INIT_JT;
				this->start_JT=false;
			}
			else {
				ROS_INFO("DarwinVisionProve: START_JT_ERROR");
				this->current_state_JT=IDLE_JT;
				//return true;				
			}
		return false;
		break;
		case SRV_INIT_JT:
			set_servo_modules_srv_.request.names.resize(2);
	 		set_servo_modules_srv_.request.modules.resize(2);
			set_servo_modules_srv_.request.names[0]="j_pan";
	  		set_servo_modules_srv_.request.names[1]="j_tilt";
	  		set_servo_modules_srv_.request.modules[0]="joints";
	  		set_servo_modules_srv_.request.modules[1]="joints";
         
			if (set_servo_modules_client_.call(set_servo_modules_srv_)){ 
				if(set_servo_modules_srv_.response.ret){
					ROS_INFO("DarwinVisionProve: JT_SUCCESS");
					n=0;
					this->current_state_JT=ACTION_INIT_JT;
					return false;
				}
				else{
					if (n<config_.n){
						ROS_INFO("DarwinVisionProve: JT NOT SUCCESS");
						this->current_state_JT=SRV_INIT_JT;
						n+=1;
					return false;
					}
					else{
						ROS_INFO("DarwinVisionProveJT: Failed to Call Server on topic set_servo_modules");
						n=0;
					return true;
					}
				}
			}
			else{
				if (n<config_.n){
					ROS_INFO("DarwinVisionProve: JT NOT SUCCESS: %i",n);
					n+=1;
					this->current_state_JT=SRV_INIT_JT;
				return false;
		
				}
				else{
					ROS_INFO("DarwinVisionProveJT: Failed to Call Server on topic set_servo_modules");
					n=0;
				return true;
				}
	
			}
		break;
		case ACTION_INIT_JT:
			joint_trajectory_goal_.trajectory.joint_names[0]="j_pan";
	  		joint_trajectory_goal_.trajectory.joint_names[1]="j_tilt";
			joint_trajectory_goal_.trajectory.points[0].positions[0]=pos_pan;
			joint_trajectory_goal_.trajectory.points[0].positions[1]=pos_tilt;
			joint_trajectory_goal_.trajectory.points[0].velocities[0]=vel_pan;
			joint_trajectory_goal_.trajectory.points[0].velocities[1]=vel_tilt;
			joint_trajectory_goal_.trajectory.points[0].accelerations[0]=0.3;
			joint_trajectory_goal_.trajectory.points[0].accelerations[1]=0.3;		

			if (n<config_.n){
				if(joint_trajectoryMakeActionRequest())
	  			{
					ROS_INFO("DarwinVisionProve: ACTION INIT");
					n=0;	
					this->current_state_JT=FINISH_JT;
				return false;						
	  			}
				else{
					ROS_INFO("DarwinVisionProve: ACTION ERROR n<5");
					n+=1;	
					this->current_state_JT=ACTION_INIT_JT;
				return false;
				}		
			}
			else{
	  			ROS_INFO("DarwinVisionProve: ACTION_INIT_ERROR");
				n=0;
				return true;
			}
	  	break;	
		case FINISH_JT:
			actionlib::SimpleClientGoalState joint_trajectory_state(actionlib::SimpleClientGoalState::PENDING);//?¿
			joint_trajectory_state=joint_trajectory_client_.getState();
			if ((joint_trajectory_state==actionlib::SimpleClientGoalState::PREEMPTED)||(joint_trajectory_state==actionlib::SimpleClientGoalState::ABORTED)){ //callback goaldone
				ROS_INFO("DarwinVisionProve: PREEMPTED||ABORTED");
				this->current_state_JT=IDLE_JT; //ha acabat de fer acció
				return true;						       
			}	
			else if(joint_trajectory_state==actionlib::SimpleClientGoalState::SUCCEEDED){
				this->current_state_JT=IDLE_JT;
				ROS_INFO("DarwinVisionProve: SUCCEEDED");
				return true;
			}

			else if (this->cancel_JT){
				ROS_INFO("DarwinVisionProve: CANCEL");
				joint_trajectory_client_.cancelGoal();
				this->current_state_JT=IDLE_JT;
				return true;
			}			
			else{
				ROS_INFO("DarwinVisionProve: ACTIVE_JT");
				this->current_state_JT=FINISH_JT;
				return false;
			}
		
		break;

	}
	
}

bool VisionProveAlgNode::HT(float pos_pan,float pos_tilt,bool startHT){ 
	this->start_HT=startHT;
	switch(this->current_state_HT)
  {
  	case IDLE_HT:
		if(this->start_HT){
			ROS_INFO("DarwinVisionProve: START_HT");
			this->current_state_HT=SRV_INIT_HT;
			this->start_HT=false;
		return false;
		}	
		else{
			ROS_INFO("DarwinVisionProve: START_HT_ERROR");
			this->current_state_HT=IDLE_HT;	
			
		}
	return false;
		
	break;
	case SRV_INIT_HT:
	set_servo_modules_srv_.request.names.resize(2);
  	set_servo_modules_srv_.request.modules.resize(2);
	set_servo_modules_srv_.request.names[0]="j_pan";
  	set_servo_modules_srv_.request.names[1]="j_tilt";
  	set_servo_modules_srv_.request.modules[0]="head";
  	set_servo_modules_srv_.request.modules[1]="head";
		if (set_servo_modules_client_.call(set_servo_modules_srv_)){ 
				if(set_servo_modules_srv_.response.ret){
					ROS_INFO("DarwinVisionProve: HT_SUCCESS");
					n=0;
					this->current_state_HT=ACTION_INIT_HT;
					return false;
				}
				else{
					if (n<config_.n){
						ROS_INFO("DarwinVisionProve: SRV_HT_NOT_SUCCESS <5");
						this->current_state_HT=SRV_INIT_HT;
						n+=1;
					return false;
					}
					else{
						ROS_INFO("DarwinVisionProveHT: Failed to Call Server on topic set_servo_modules");
						n=0;
					return true;
					}
				}
		return false;			
		}
		else{
			if (n<config_.n){
				ROS_INFO("DarwinVisionProve: SRV_HT_NOT_SUCCESS <5: %i",n);
				n+=1;
				this->current_state_HT=SRV_INIT_HT;
			return false;
			}
			else{
				ROS_INFO("DarwinVisionProveHT: Failed to Call Server on topic set_servo_modules");
				n=0;
			return true;
			}
			
		}
	break;
	case ACTION_INIT_HT:
		head_follow_target_goal_.target_pan=pos_pan;
  		head_follow_target_goal_.target_tilt=pos_tilt;
		if (n<config_.n){
			if(head_follow_targetMakeActionRequest()) 
			{
				ROS_INFO("DarwinVisionProve: ACTION_HT_INIT");
				n=0;
				
				this->current_state_HT=FINISH_HT;
			return false;					
  			}
			else{
				ROS_INFO("DarwinVisionProve: ACTION_HT_ERROR<5");
				n+=1;	
				this->current_state_HT=ACTION_INIT_HT;
			return false;
									
			}		
		}
		else{
  			ROS_INFO("DarwinVisionProve: ACTION_INIT_HT_ERROR");
			n=0;
			//do someting
                 	//break;
		 	//error
		return true;
		}
	break;
	case FINISH_HT:
		this->head_target_JointTrajectoryPoint_msg_.positions[0]=pos_pan;
  		this->head_target_JointTrajectoryPoint_msg_.positions[1]=pos_tilt;
 		this->head_target_JointTrajectoryPoint_msg_.velocities[0]=1.5;
		this->head_target_JointTrajectoryPoint_msg_.velocities[1]=1.5;
		this->head_target_publisher_.publish(this->head_target_JointTrajectoryPoint_msg_);

		actionlib::SimpleClientGoalState head_follow_target_state(actionlib::SimpleClientGoalState::PENDING);
		head_follow_target_state=head_follow_target_client_.getState();
		if ((head_follow_target_state==actionlib::SimpleClientGoalState::PREEMPTED)||(head_follow_target_state==actionlib::SimpleClientGoalState::ABORTED)){ //callback goalnotdone
			ROS_INFO("DarwinVisionProve: PREEMPTED||ABORTED");
			this->current_state_HT=IDLE_HT; 
			return true;
		}
		//No entrará mai a succeeded!
		else if (this->config_.CANCEL){
			ROS_INFO("DarwinVisionProve: CANCEL");
			head_follow_target_client_.cancelGoal();
			this->cancel_HT=false;
			this->current_state_HT=IDLE_HT;
			return true;
		}		
		else{
			ROS_INFO("DarwinVisionProve: ACTIVE_HT");
			this->current_state_HT=FINISH_HT;
			return false;
		}
	break;
  }		
}
bool VisionProveAlgNode::WK(float linear_x,float linear_y,float angular_z,bool startWK,bool cancelWK){
	this->cancel_WK=cancelWK;
	this->start_WK=startWK;

	switch(this->current_state_WK)
  {
  	case IDLE_WK:
		if(this->start_WK){
			this->start_WK=false;
			ROS_INFO("DarwinVisionProve: START_WK");
			n=0;
			this->current_state_WK=SRV_INIT_WK;
		}	
		else{
			ROS_INFO("DarwinVisionProve: START_WK_ERROR");
			this->current_state_WK=IDLE_WK;	
		}
	return false;
	break;
	case SRV_INIT_WK:
		set_servo_modules_srv_.request.names.resize(18);
	 	set_servo_modules_srv_.request.modules.resize(18);
		set_servo_modules_srv_.request.names[0]="j_ankle_pitch_l";
		set_servo_modules_srv_.request.names[1]="j_ankle_pitch_r";
		set_servo_modules_srv_.request.names[2]="j_ankle_roll_l";
		set_servo_modules_srv_.request.names[3]="j_ankle_roll_r";
		set_servo_modules_srv_.request.names[4]="j_shoulder_roll_l";
		set_servo_modules_srv_.request.names[5]="j_shoulder_roll_r";
		set_servo_modules_srv_.request.names[6]="j_knee_l";
		set_servo_modules_srv_.request.names[7]="j_knee_r";
	  set_servo_modules_srv_.request.names[8]="j_elbow_l";
	  set_servo_modules_srv_.request.names[9]="j_elbow_r";
	  set_servo_modules_srv_.request.names[10]="j_hip_pitch_l";
		set_servo_modules_srv_.request.names[11]="j_hip_pitch_r";
		set_servo_modules_srv_.request.names[12]="j_hip_roll_l";
		set_servo_modules_srv_.request.names[13]="j_hip_roll_r";
		set_servo_modules_srv_.request.names[14]="j_hip_yaw_l";
		set_servo_modules_srv_.request.names[15]="j_hip_yaw_r";
		set_servo_modules_srv_.request.names[16]="j_shoulder_pitch_l";
		set_servo_modules_srv_.request.names[17]="j_shoulder_pitch_r";
		for (i = 0; i <18 ; ++i){
  			set_servo_modules_srv_.request.modules[i]="walking";
  		}
		if (set_servo_modules_client_.call(set_servo_modules_srv_)){
			if(set_servo_modules_srv_.response.ret){
				ROS_INFO("DarwinVisionProve: WK_SUCCESS");
				n=0;
				this->current_state_WK=SRV_WK_PARAMS;
				return false;
			}
			else{
				if (n<config_.n){
					ROS_INFO("DarwinVisionProve: SRV_WK_NOT_SUCCESS<5");
					this->current_state_WK=SRV_INIT_WK;
					n+=1;
				return false;
				}
				else{
					ROS_INFO("DarwinVisionProveWK: Failed to Call Server on topic set_servo_modules");
					n=0;
				return true;
				}

			}
			
		}
		else{
			if (n<config_.n){
				ROS_INFO("DarwinVisionProve: SRV_WK_NOT_SUCCESS: %i",n);
				this->current_state_WK=SRV_INIT_WK;
				n+=1;
			return false;
			}
			else{
				ROS_INFO("DarwinVisionProveWK: Failed to Call Server on topic set_servo_modules");
				n=0;
			return true;
			}
			
		}
	break;
	case SRV_WK_PARAMS:
	 	set_walk_params_srv_.request.params.Y_SWAP_AMPLITUDE = 0.02; 
		set_walk_params_srv_.request.params.Z_SWAP_AMPLITUDE = 0.005; 
		set_walk_params_srv_.request.params.ARM_SWING_GAIN = 1.5; 
		set_walk_params_srv_.request.params.PELVIS_OFFSET = 0.05; 
		set_walk_params_srv_.request.params.HIP_PITCH_OFFSET = 0.23; 
		set_walk_params_srv_.request.params.X_OFFSET = -0.01; 
		set_walk_params_srv_.request.params.Y_OFFSET = 0.005; 
		set_walk_params_srv_.request.params.Z_OFFSET = 0.02; 
		set_walk_params_srv_.request.params.A_OFFSET = 0.0; 
		set_walk_params_srv_.request.params.P_OFFSET = 0.0; 
		set_walk_params_srv_.request.params.R_OFFSET = 0.0; 
		set_walk_params_srv_.request.params.PERIOD_TIME = 0.6; 
		set_walk_params_srv_.request.params.DSP_RATIO = 0.1; 
		set_walk_params_srv_.request.params.STEP_FB_RATIO = 0.28; 
		set_walk_params_srv_.request.params.FOOT_HEIGHT = 0.04; 
		set_walk_params_srv_.request.params.MAX_VEL = 0.01; 
		set_walk_params_srv_.request.params.MAX_ROT_VEL = 0.5; //acceleració de rotació 
		ROS_INFO("DarwinVisionProve:: Sending New Request!"); 
		if(set_walk_params_client_.call(set_walk_params_srv_)) 
		{
			if(set_walk_params_srv_.response.ret){
			 	ROS_INFO("DarwinVisionProve:: Parameters WK changed successfully"); 
				this->current_state_WK=MSG_WK;
				n=0;
			return false;
			}
			else{
				if (n<config_.n){
					ROS_INFO("DarwinVisionProve:: Reintented to change WK parameters"); 
					this->current_state_WK=SRV_WK_PARAMS;
					n+=1;
				return false;
				}
				else{
					ROS_INFO("DarwinVisionProveWK:: Failed to Call Server on topic set_walk_params"); 
					n=0;
			 	return true;
				}
			}
		}
		else
		{
			if (n<config_.n){
				ROS_INFO("DarwinVisionProve::  Reintented to change WK parameters"); 
				this->current_state_WK=SRV_WK_PARAMS;
				n+=1;
			return false;
			}
			else{
				ROS_INFO("WDarwinVisionProveWK:: Failed to Call Server on topic set_walk_params ");
				n=0;
			return true;
			}
		     	
		}	   
	break;
	case MSG_WK:
    		this->cmd_vel_Twist_msg_.angular.z=angular_z;
		this->cmd_vel_Twist_msg_.linear.x=linear_x;
		this->cmd_vel_Twist_msg_.linear.y=linear_y;
		this->cmd_vel_publisher_.publish(this->cmd_vel_Twist_msg_);
		ROS_INFO("DarwinVisionProve:  WK");
		if (cancel_WK){
			this->start_wait_time=ros::Time::now();
			this->current_state_WK=FINISH_WK;
			ROS_INFO("DarwinVisionProve:TRYING STOPING");
			//ROS_INFO_STREAM(id);
		}
	return false;
	break;
	case FINISH_WK:
		this->cmd_vel_Twist_msg_.angular.z=0.00000000000;
		this->cmd_vel_Twist_msg_.linear.x=0.0;
		this->cmd_vel_Twist_msg_.linear.y=0.0;
		wait_timeout=ros::Time::now()-start_wait_time;
		ROS_INFO("time:");
		ROS_INFO_STREAM(wait_timeout.toSec());
		ROS_INFO("vel.ang.z: %f", this->cmd_vel_Twist_msg_.angular.z);
		this->cmd_vel_publisher_.publish(this->cmd_vel_Twist_msg_);
		if (wait_timeout.toSec()>0.9){
			this->cmd_vel_Twist_msg_.angular.z=0.00000000;
			this->cmd_vel_publisher_.publish(this->cmd_vel_Twist_msg_);
			this->current_state_WK=IDLE_WK;
			ROS_INFO("DarwinVisionProve: STOP WK");
		return true;
		}
		else{
			this->current_state_WK=FINISH_WK;
			ROS_INFO("DarwinVisionProve:REINTENTED STOPPING");
		return false;
		}
	
	break;
  }
}
bool VisionProveAlgNode::AC(int type_action,bool startAC,bool cancelAC){ 
	this->start_AC=startAC;
	this->cancel_AC=cancelAC;
	switch(this->current_state_AC)
	{
	  	case IDLE_AC:
				if(this->config_.START){ 
					ROS_INFO("DarwinVisionProve: START_ACTION");
					this->current_state_AC=SRV_INIT_AC;
					this->start_AC=false;
				}
				else {
					ROS_INFO("DarwinVisionProve: START_AC_ERROR");
					this->current_state_AC=IDLE_AC;
					//return true;				
				}
		return false;
		break;
		case SRV_INIT_AC:
		set_servo_modules_srv_.request.names.resize(32);
	 	set_servo_modules_srv_.request.modules.resize(32);
		set_servo_modules_srv_.request.names[0]="j_ankle_pitch_l";//
		set_servo_modules_srv_.request.names[1]="j_ankle_pitch_r";//
		set_servo_modules_srv_.request.names[2]="j_ankle_roll_l";
		set_servo_modules_srv_.request.names[3]="j_ankle_roll_r";//
		set_servo_modules_srv_.request.names[4]="j_shoulder_roll_l";//
		set_servo_modules_srv_.request.names[5]="j_shoulder_roll_r";//
		set_servo_modules_srv_.request.names[6]="j_knee_l";//
		set_servo_modules_srv_.request.names[7]="j_knee_r";//
	 	set_servo_modules_srv_.request.names[8]="j_elbow_l";//
	 	set_servo_modules_srv_.request.names[9]="j_elbow_r";//
	 	set_servo_modules_srv_.request.names[10]="j_hip_pitch_l";//
		set_servo_modules_srv_.request.names[11]="j_hip_pitch_r";//
		set_servo_modules_srv_.request.names[12]="j_hip_roll_l";//
		set_servo_modules_srv_.request.names[13]="j_hip_roll_r";//
		set_servo_modules_srv_.request.names[14]="j_hip_yaw_l";//
		set_servo_modules_srv_.request.names[15]="j_hip_yaw_r";//
		set_servo_modules_srv_.request.names[16]="j_shoulder_pitch_l";//
		set_servo_modules_srv_.request.names[17]="j_shoulder_pitch_r";//
    		set_servo_modules_srv_.request.names[18]="j_pan";//
    		set_servo_modules_srv_.request.names[19]="j_tilt";//
		/*set_servo_modules_srv_.request.names[20]="Servo0";//
		set_servo_modules_srv_.request.names[21]="Servo21";//
		set_servo_modules_srv_.request.names[22]="Servo22";//
		set_servo_modules_srv_.request.names[23]="Servo23";//
		set_servo_modules_srv_.request.names[24]="Servo24";//
		set_servo_modules_srv_.request.names[25]="Servo25";//
		set_servo_modules_srv_.request.names[26]="Servo26";//
		set_servo_modules_srv_.request.names[27]="Servo27";//
		set_servo_modules_srv_.request.names[28]="Servo28";//
		set_servo_modules_srv_.request.names[29]="Servo29";//
		set_servo_modules_srv_.request.names[30]="Servo30";//
		set_servo_modules_srv_.request.names[31]="Servo31";//*/

		for (i = 0; i <32 ; ++i){
  			set_servo_modules_srv_.request.modules[i]="action";
  		}
		if (set_servo_modules_client_.call(set_servo_modules_srv_)){
			if(set_servo_modules_srv_.response.ret){
				ROS_INFO("DarwinVisionProve: ACTION_SRV_SUCCESS");
				n=0;
				this->current_state_AC=ACTION_INIT_AC;
				return false;
			}
			else{
				if (n<config_.n){
					ROS_INFO("DarwinVisionProve: SRV_AC_NOT_SUCCESS<5");
					this->current_state_AC=SRV_INIT_AC;
					n+=1;
				return false;
				}
				else{
					ROS_INFO("DarwinVisionProve: AC: FAILED TO CALL SERVER ON TOPIC SET_SERVO_MODULES");
					n=0;
				return true;
				}
			}
			
		}
		else{
			if (n<config_.n){
				ROS_INFO("DarwinVisionProve: SRV_AC_NOT_SUCCESS<5: %i",n);
				this->current_state_AC=SRV_INIT_AC;
				n+=1;
			return false;
			}
			else{
				ROS_INFO("DarwinVisionProve: AC: FAILED TO CALL SERVER ON TOPIC SET_SERVO_MODULES");
				n=0;
			return true;
			}
			
		}
		break;
		case ACTION_INIT_AC:
			  
			motion_action_goal_.motion_id=type_action;
			if (n<config_.n){
				if(motion_actionMakeActionRequest())
	  			{
					ROS_INFO("DarwinVisionProve: ACTION_AC_INIT");
					n=0;	
					this->current_state_AC=FINISH_AC;
				return false;						
	  			}
				else{
					ROS_INFO("DarwinVisionProve: ACTION_AC_ERROR<5");
					n+=1;	
					this->current_state_AC=ACTION_INIT_AC;
				return false;
				}		
			}
			else{
	  			ROS_INFO("DarwinVisionProve: ACTION_INIT_AC_ERROR");
				n=0;
				return true;
			}
	  	break;	
		case FINISH_AC:
			actionlib::SimpleClientGoalState motion_action_state(actionlib::SimpleClientGoalState::PENDING);
			motion_action_state=motion_action_client_.getState();
			if ((motion_action_state==actionlib::SimpleClientGoalState::PREEMPTED)||(motion_action_state==actionlib::SimpleClientGoalState::ABORTED)){ //callback goaldone
				ROS_INFO("DarwinVisionProve: PREEMPTED||ABORTED");
				this->current_state_AC=IDLE_AC; //ha acabat de fer acció
				return true;						       
			}	
			else if(motion_action_state==actionlib::SimpleClientGoalState::SUCCEEDED){
				this->current_state_AC=IDLE_AC;
				ROS_INFO("DarwinVisionProve: SUCCEEDED");
				return true;
			}

			else if (this->config_.CANCEL){
				ROS_INFO("DarwinVisionProve: CANCEL");
				motion_action_client_.cancelGoal();
				this->config_.CANCEL=false;
				this->current_state_AC=IDLE_AC;
				return true;
			}			
			else{
				ROS_INFO("DarwinVisionProve: ACTIVE_AC");
				this->current_state_AC=FINISH_AC;
				return false;
			}
		
		break;

	}
	
}

bool VisionProveAlgNode::QR_exists(std::vector<std::string> vec, std::string id){
				int j=0;
				for (j = 0; j <vec.size() ; ++j){
		 							if (vec[j]==id){
											return true;
									}	
				}
				return false;
}

bool VisionProveAlgNode::cerca_QR(std::string id_to_find){
	switch(this->current_state_MH)
	{
	case MOVE_HEAD_L:
			ROS_INFO("angle_pan_act: %f",this->angle_pan_act);
			ROS_INFO("angle_tilt_act: %f",this->angle_tilt_act);
			if(!JT(this->angle_pan_act+2*c_pan,this->angle_tilt_act+1*c_tilt_l,2,2,true,false)){ //posar cancel quan trobi QR;//factors n;
		
				if (n_QR!=0){
					if ((id_to_find=="")||(id==id_to_find)){ 
							if(!JT(this->angle_pan_act+2*c_pan,this->angle_tilt_act+1*c_tilt_l,2,2,false,true)){
											this->current_state_MH=MOVE_HEAD_L;
											ROS_INFO("QR FOUNDED");
											return false;
							}
							else{
										this->current_state_MH=MOVE_HEAD_L;
										ROS_INFO("MOVE_HEAD CANCELLED");
										y=0;
										return true;
							}
					}
					else{
										this->current_state_MH=MOVE_HEAD_L;
										ROS_INFO(" QR IDENTIFIED, BUT NOT WHAT WE WANT!");	
										return false;
					}
				}
			}
			else{
				if (y<config_.n){
					this->current_state_MH=MOVE_HEAD_R;
					ROS_INFO("Trying to move head R");
					ROS_INFO("angle_pan: %f",angle_pan_act-2*c_pan);
					ROS_INFO("angle_tilt: %f",angle_tilt_act-1*c_tilt_l);
					this->start_JT=true;
					this->current_state_JT=IDLE_JT;
					return false;

				}
				else{
					c_pan=0.15;
					c_tilt_l=0.01;
					c_tilt_r=0.01;
					//y=0;
					this->current_state_MH=MOVE_HEAD_L;
					return true;
				}
		}
		
	break;
 
	case MOVE_HEAD_R:
			ROS_INFO("angle_pan_act: %f",this->angle_pan_act);
			ROS_INFO("angle_tilt_act: %f",this->angle_tilt_act);
			if(!JT(this->angle_pan_act-2*c_pan,this->angle_tilt_act-1*c_tilt_r,2,2,true,false)){ //posar cancel quan trobi QR;//factors n;
				if (n_QR!=0){
					if ((id_to_find=="")||(id==id_to_find)){
						if(!JT(this->angle_pan_act-2*c_pan,this->angle_tilt_act-1*c_tilt_r,2,2,false,true)){
										this->current_state_MH=MOVE_HEAD_R;
										ROS_INFO("Move_head_R_cancel");
										return false;
						}
						else{
										this->current_state_MH=MOVE_HEAD_R;
										ROS_INFO("MOVE_HEAD CANCELLED");
										n=0;
										return true;
						}
					}
					else{
										this->current_state_MH=MOVE_HEAD_R;
										ROS_INFO(" QR IDENTIFIED, BUT NOT WHAT WE WANT!");	
										return false;
					}
				}
			}
			else{
				if (y<config_.y){
					this->current_state_MH=MOVE_HEAD_L;
					ROS_INFO("c_pan: %f",c_pan);
					ROS_INFO("c_tilt_l: %f",c_tilt_l);
					c_pan+=0.15;
					c_tilt_l+=0.03;
					c_tilt_r+=0.03;
					y=y+1;
					ROS_INFO("Trying to move head L");
					ROS_INFO("angle_pan: %f",angle_pan_act+4*c_pan);
					ROS_INFO("angle_tilt: %f",angle_tilt_act+2*c_tilt_r);
					ROS_INFO("y: %i",y);
					this->start_JT=true;
					return false;
				}
				else{
					c_pan=0.15;
					c_tilt_l=0.01;
					c_tilt_r=0.01;
					y=0;
					this->current_state_MH=MOVE_HEAD_L;
					return true;
				}
				
			}
		
	break;
}
}

float VisionProveAlgNode::angle_spin_fctn(std::string id1){
		float angle_spin1=0.0;
		float PI=3.14159265;
		if (id1=="Turn45R"){
          angle_spin1=-45;
        }
        else if (id1=="Turn90R"){
          angle_spin1=-90;
        }
        else if (id1=="Turn180R"){
          angle_spin1=-180;
        }
		else if (id1=="Turn135R"){
          angle_spin1=-135;
        }
        else if (id1=="Turn270R"){
          angle_spin1=-270;
        }
        else if (id1=="Turn360R"){
          angle_spin1=-360;
        }
        else if (id1=="Turn45L"){
          angle_spin1=+45;
        }
        else if (id1=="Turn90L"){
          angle_spin1=+90;
        }
	else if (id1=="Turn135L"){
          angle_spin1=+135;
        }
        else if (id1=="Turn180L"){
          angle_spin1=+180;
        }
        else if (id1=="Turn270L"){
          angle_spin1=+270;
        }
        else if (id1=="Turn360L"){
          angle_spin1=+360;
        }
        angle_spin1=angle_spin1*(PI/180);
return angle_spin1;
}

int VisionProveAlgNode::centering(std::vector<float> angle_x){
			int j=0;
		 	float value=angle_x[0];
			int pos=j;
			for (j = 0; j <angle_x.size() ; ++j){
		 					if (angle_x[j]<value){
									value=angle_x[j];
									pos=j;
							}
			}
return pos;
}

std::vector<int> VisionProveAlgNode::find_QR(std::vector<std::string> ex, std::vector<std::string> ides){
	int j=0;
	int l=0;
	std::vector<int> QRs;
		for ( l=0; l<ides.size(); ++l){
            if (!QR_exists(ex,ides[l])){
                QRs.push_back(l);
			}
		}
	return QRs;
}
